
class MnistNetClassesUtil():
    
    def __init__(self):
        self.index_to_name = {0:'benign keratosis-like lesions', 1:'melanoma',2:'melanocytic nevi'}
        self.name_to_index = {'benign keratosis-like lesions':0, 'melanoma':1,'melanocytic nevi':2}
        self.index_to_code = {0:'bkl', 1:'mel', 2:'nv'}
    
    def get_name_from_index(self, index):
        return self.index_to_name[index]
    
    def get_index_from_name(self, name):
        return self.name_to_index[name]
    
    def get_code_from_index(self, index):
        return self.index_to_code[index]
    
    def get_index_from_code(self, code):
        return self.code_to_index[code]

    def get_name_from_code(self, code):
        return self.index_to_name[self.code_to_index[code]]
    
    def get_code_from_name(self, name):
        return self.index_to_code[self.name_to_index[name]]
