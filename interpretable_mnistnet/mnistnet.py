from matplotlib import image
import tensorflow as tf
import numpy as np
import cv2
import scipy as sp
import math

def load_raw_image(image_path):
    img = cv2.imread(image_path)
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

def load_cropped_image(image_path):
    img = load_raw_image(image_path)
    #state crop and original dims
    crop_width = 224
    crop_height = 224
    width, height = img.shape[1], img.shape[0]

    aspect_ratio = width/height
    if (width < height):
        resize_width = crop_width
        resize_height = int(resize_width / aspect_ratio)
    else:
        resize_height = crop_height
        resize_width = int(resize_height * aspect_ratio)
    resize_img = cv2.resize(img, (resize_width+1, resize_height+1), interpolation=cv2.INTER_AREA)
    
    #determine center of original image half crop width / height
    mid_x, mid_y = int(resize_width/2) + 1, int(resize_height/2) + 1
    cw2, ch2 = int(crop_width/2), int(crop_height/2)
    
    crop_img = resize_img[mid_y-ch2:mid_y+ch2+1, mid_x-cw2:mid_x+cw2]
    return crop_img

 

class MnistNet():

    def __init__(self, model_path="MNIST_BASIC_CNN.h5", target_name = 'conv2d_5/Relu:0'):
        sess = tf.compat.v1.Session()
        tf.compat.v1.keras.backend.set_session(sess)
        init_op = tf.compat.v1.global_variables_initializer()
        sess.run(init_op)
        self.sess = sess
        self.graph = sess.graph
        self.model = tf.keras.models.load_model(model_path)

        #tensors
        self.image_tensor = self.graph.get_tensor_by_name("input_1:0")
        self.target_tensor = self.graph.get_tensor_by_name(target_name)
        self.out_tensor = self.graph.get_tensor_by_name('softmax/Softmax:0')
    
    def get_input_as_array(self,image_path):
        return load_cropped_image(image_path)
    
    def summary(self):
        self.model.summary()
    
    def get_prediction(self, image_dir):
        img = load_cropped_image(image_dir)
        return self.model.predict([[img]])
 
    def get_activations(self, image_dir):
        img = load_cropped_image(image_dir)
        activations = self.sess.run([self.target_tensor], feed_dict={self.image_tensor:[img]})
        max_activation = np.max(activations)
        return activations[0][0], max_activation
    
    def get_activations_for_dict(self, image_dir, no_features = 4):
        acts, max_act= self.get_activations(image_dir)
        cropped_acts = [[[{"n": n.tolist(), "v": float(act_vec[n])} for n in np.flip(np.argsort(act_vec)[-no_features:])] for act_vec in act_slice] for act_slice in acts]
        return cropped_acts, max_act
        
    def get_scores(self, image_dir, class_index = 0):
        img = load_cropped_image(image_dir)

        #compute grads
        one_hot = tf.sparse_to_dense(class_index, [3], 1.0)
        signal = tf.multiply(self.out_tensor, one_hot)
        loss = tf.reduce_mean(signal)
        grads = tf.gradients(loss, self.target_tensor)[0]
        
        #compute scores
        output, grads_val = self.sess.run([self.target_tensor, grads], feed_dict={self.image_tensor:[img]})
        class_score = np.multiply(output, grads_val)[0]
        return class_score, np.max(class_score)
    
    def get_scores_for_dict(self, image_dir, class_index = 0, no_features = 4):
        scores, max_score = self.get_scores(image_dir, class_index=class_index)
        cropped_scores = [[[{"n": n.tolist(), "v": float(score_vec[n])} for n in np.flip(np.argsort(score_vec)[-no_features:])] for score_vec in score_slice] for score_slice in scores]
        return cropped_scores, max_score

    def get_smooth_scores(self, image_dir, class_index = 0):
        interpolated_images = self.interpolate_images(image_dir)

        #compute grads
        one_hot = tf.sparse_to_dense(class_index, [3], 1.0)
        signal = tf.multiply(self.out_tensor, one_hot)
        loss = tf.reduce_mean(signal)
        grads = tf.gradients(loss, self.target_tensor)[0]

        #output scores
        acts, grads_vals = self.sess.run([self.target_tensor, grads], feed_dict={self.image_tensor:interpolated_images})

        grads_by_vals = np.multiply(acts, grads_vals)
        smooth_scores = np.mean(grads_by_vals, axis=0)
        return smooth_scores, np.max(smooth_scores)
    
    def get_smooth_score_for_dict(self, image_dir, class_index = 0, no_features = 4):
        scores, max_score = self.get_smooth_scores(image_dir, class_index=class_index)
        cropped_scores = [[[{"n": n.tolist(), "v": float(score_vec[n])} for n in np.flip(np.argsort(score_vec)[-no_features:])] for score_vec in score_slice] for score_slice in scores]
        return cropped_scores, max_score 
    
    def interpolate_images(self, image_dir, no_interpolations = 5):
        img = load_cropped_image(image_dir)
        interpolated_images = []
        for i in range(1,no_interpolations+1):
            interpolated_images.append(img * (i/(no_interpolations)))
        return interpolated_images
    
    def get_integrated_image_gradients(self, image_dir, class_index = 0, no_interpolations = 5,):
        interpolated_images = self.interpolate_images(image_dir, no_interpolations=no_interpolations)
        #get activations for batch 
        one_hot = tf.sparse_to_dense(class_index, [3], 1.0)
        signal = tf.multiply(self.out_tensor, one_hot)
        loss = tf.reduce_mean(signal)
        grads = tf.gradients(loss, self.image_tensor)[0]
        grads_val = self.sess.run([grads], feed_dict={self.image_tensor:interpolated_images})

        grads_by_vals = np.multiply(interpolated_images, grads_val)
        weights = np.mean(grads_by_vals[0], axis=0)
        reduce_color_axis = np.mean(weights, axis=-1)
        truncated = np.maximum(reduce_color_axis, 0)
        normalized = truncated / np.amax(truncated)
        shape = np.shape(normalized)
        overlay = cv2.cvtColor(cv2.applyColorMap(np.uint8(255*normalized), cv2.COLORMAP_MAGMA), cv2.COLOR_BGR2RGB)

        #generate image
        raw_image = load_cropped_image(image_dir)
        return raw_image, overlay, overlay*0.6 + raw_image * 0.4


    # def get_scores_partial_run_test(self, image_dir, class_index = 0, target_name='mixed9/concat:0'):
    #     img = load_img(image_dir)

    #     #compute grads
    #     one_hot = tf.sparse_to_dense(class_index, [1000], 1.0)
    #     signal = tf.multiply(self.out_tensor, one_hot)
    #     loss = tf.reduce_mean(signal)
    #     grads = tf.gradients(loss, self.target_tensor)[0]
        
    #     #compute scores
    #     output, grads_val = self.sess.run([self.target_tensor, grads], feed_dict={self.image_tensor:[img]})
    #     class_score = np.multiply(output, grads_val)[0]
    #     #compute second grads
    #     grads_val_2 = self.sess.run([grads], feed_dict={self.target_tensor:output})
    #     if (np.array_equal(grads_val, grads_val_2)):
    #         print("equal")
    #     else:
    #         print("not equal")
    #         print(np.max(grads_val))
    #         print(np.max(grads_val_2))
    #         print(np.min(grads_val))
    #         print(np.min(grads_val_2))
    #         print(np.max(grads_val - grads_val_2))
    #     #return class_score, np.max(class_score)
    
    # def get_preds_partial_run_test(self, image_dir, class_index = 0, target_name='mixed9/concat:0'):
    #     img = load_img(image_dir)
        
    #     #compute scores
    #     output= self.sess.run([self.target_tensor], feed_dict={self.image_tensor:[img]})
    #     preds = self.sess.run([self.out_tensor], feed_dict={self.target_tensor:output[0]})
    #     preds_2 = self.get_prediction(image_dir)
    #     print(decode_predictions(preds[0]))
    #     print(preds_2)
        